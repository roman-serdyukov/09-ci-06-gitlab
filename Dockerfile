FROM centos:7

RUN yum install python3 python3-pip -y 
RUN pip3 install flask flask-jsonpify flask-restful

COPY api.py /python-api/api.py


CMD ["python3", "/python-api/api.py"]
